#ifndef GT_CONFIG_H
#define GT_CONFIG_H
#define GT_CC "cc (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
#define GT_CFLAGS " -g -Wall -Wunused-parameter -pipe -fPIC -Wpointer-arith -Wno-unknown-pragmas -O3 -Werror"
#define GT_CPPFLAGS " -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -DHAVE_MEMMOVE -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -DLUA_DL_DLOPEN -DLUA_USE_MKSTEMP -DSQLITE_THREADSAFE=0 -DHAVE_SQLITE -I/home/hibberdm/data_deposition/genometools/src -I/home/hibberdm/data_deposition/genometools/obj -I/home/hibberdm/data_deposition/genometools/src/external/zlib-1.2.8 -I/home/hibberdm/data_deposition/genometools/src/external/md5-1.2/src -I/home/hibberdm/data_deposition/genometools/src/external/lua-5.1.5/src -I/home/hibberdm/data_deposition/genometools/src/external/luafilesystem-1.5.0/src -I/home/hibberdm/data_deposition/genometools/src/external/lpeg-0.10.2 -I/home/hibberdm/data_deposition/genometools/src/external/expat-2.0.1/lib -I/home/hibberdm/data_deposition/genometools/src/external/bzip2-1.0.6 -I/home/hibberdm/data_deposition/genometools/src/external/samtools-0.1.18 -I/home/hibberdm/data_deposition/genometools/src/external/sqlite-3.33.0 -I/home/hibberdm/data_deposition/genometools/src/external/tre/include -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng12 -I/usr/include/pango-1.0 -I/usr/include/harfbuzz -I/usr/include/pango-1.0 -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng12 -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/home/hibberdm/data_deposition/genometools/src/external/sqlite-3.33.0"
#define GT_VERSION "1.6.1"
#define GT_MAJOR_VERSION 1
#define GT_MINOR_VERSION 6
#define GT_MICRO_VERSION 1
#endif
