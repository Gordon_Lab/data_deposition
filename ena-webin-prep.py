#!/usr/bin/env python3

import sys
import os
import errno
import argparse
#import hashlib
#from Bio import SeqIO
#import subprocess
#import shutil

parser = argparse.ArgumentParser()
parser.add_argument("-m", help="[STRING] Input mapping file", dest="mapfile", required=True)
parser.add_argument("-g", help="[STRING] Genome directory (contains gff files)", dest="genome_dir")
parser.add_argument("-r", help="[STRING] Reads directory", dest="reads_dir")
args = parser.parse_args()

if args.genome_dir and args.reads_dir:
	sys.exit("[ERROR] Must specify EITHER genome_dir OR reads_dir")

#command = 'bowtie-build'
#if not (shutil.which(command) is not None):
#	exit("ERROR: " + command + " is not available!")

manifest_dir = "manifests"
if not os.path.exists(manifest_dir):
	os.mkdir(manifest_dir)

mapping_file = open( args.mapfile, 'r')
mapping_lines = mapping_file.readlines()
first = True
headers = list()
manifest_jobs = dict()
submission_jobs = set()
accessions = set()

genome_fields = ["study_acc", "sample_acc", "locus_tag", "taxid", "genome_type",
	"classification", "translation_table", "genus", "species", "strain",
	"internal_acc", "assembly_type", "coverage", "assembler", "platform"]
reads_fields = ["study_acc", "sample_acc", "experiment_name", "platform", "library_source",
	"library_selection", "library_strategy", "fastq_1", "fastq_2"]

mapping_file_format = open( os.path.splitext(args.mapfile)[0] + "_format" + os.path.splitext(args.mapfile)[1], 'w')

for line in mapping_lines:
	if line.startswith('#'):
		continue
	else:
		stripline = line.strip()
		splitline = stripline.split('\t')
		if (first):
			headers = splitline
			first = False
			if (args.genome_dir):
				mapping_file_format.write("\t".join(genome_fields) + "\n")
			elif (args.reads_dir):
				mapping_file_format.write("\t".join(reads_fields) + "\n")
		elif (args.genome_dir):

#			sys.stdout.write("[STATUS] The user has requested creation of manifest files for GENOME submission.\n")

			study_acc = splitline[headers.index(genome_fields[0])]
			sample_acc = splitline[headers.index(genome_fields[1])]
			genome_type = splitline[headers.index(genome_fields[4])]
			classification = splitline[headers.index(genome_fields[5])]
			translation_table = splitline[headers.index(genome_fields[6])]
			genus = splitline[headers.index(genome_fields[7])]
			species = splitline[headers.index(genome_fields[8])]
			strain = splitline[headers.index(genome_fields[9])]
			internal_accession = splitline[headers.index(genome_fields[10])]
			assembly_type = splitline[headers.index(genome_fields[11])]
			taxid = splitline[headers.index(genome_fields[3])]
			locus_tag = splitline[headers.index(genome_fields[2])]
			coverage = splitline[headers.index(genome_fields[12])]
			assembler = splitline[headers.index(genome_fields[13])]
			platform = splitline[headers.index(genome_fields[14])]

			if not os.path.isfile(os.path.join(args.genome_dir, internal_accession + ".embl.gz")):
				sys.exit("[ERROR] File {} not found in {}!".format(internal_accession + ".embl.gz", args.genome_dir))

			manifest_jobs[internal_accession] = ("STUDY" + "\t" + study_acc + "\n" \
							  					 "SAMPLE" + "\t" + sample_acc + "\n" \
												 "ASSEMBLYNAME" + "\t" + internal_accession + "\n" \
												 "ASSEMBLY_TYPE" + "\t" + assembly_type + "\n" \
                                                                                                 "COVERAGE" + "\t" + coverage + "\n" \
												 "PROGRAM" + "\t" + assembler + "\n" \
												 "PLATFORM" + "\t" + platform + "\n" \
												 "MOLECULETYPE" + "\t" + "genomic DNA" + "\n" \
												 "FLATFILE" + "\t" + internal_accession + ".embl.gz" + "\n" )

			accessions.add(internal_accession)

			mapping_file_format.write("\t".join([study_acc, sample_acc, locus_tag, taxid, genome_type, classification, translation_table, genus, species, strain, internal_accession, assembly_type, coverage, assembler, platform]) + "\n")

		elif (args.reads_dir):

#			sys.stdout.write("[STATUS] The user has requested creation of manifest files for READ FILE submission.\n")

			study_acc = splitline[headers.index(reads_fields[0])]
			sample_acc = splitline[headers.index(reads_fields[1])]
			experiment_name = splitline[headers.index(reads_fields[2])]
			platform = splitline[headers.index(reads_fields[3])]
			library_source = splitline[headers.index(reads_fields[4])]
			library_selection = splitline[headers.index(reads_fields[5])]
			library_strategy = splitline[headers.index(reads_fields[6])]
			fastq_1 = splitline[headers.index(reads_fields[7])]
			if "fastq_2" in headers:
				try:
					fastq_2 = splitline[headers.index(reads_fields[8])]
				except IndexError:
    					sys.exit("[ERROR] No fastq_2 file supplied for sample {}!".format(sample_acc))
			else:
				fastq_2 = ""

			if not os.path.isfile(os.path.join(args.reads_dir, fastq_1)):
				sys.exit("[ERROR] fastq_1 file {} not found in {}!".format(fastq_1, args.reads_dir))
			if fastq_2 != "" and not os.path.isfile(os.path.join(args.reads_dir, fastq_2)):
				sys.exit("[ERROR] fastq_2 file {} not found in {}!".format(fastq_2, args.reads_dir))

			fastq_string = "FASTQ" + "\t" + fastq_1

			if fastq_2 != "":
				fastq_string = fastq_string + "\n" + "FASTQ" + "\t" + fastq_2

			manifest_jobs[sample_acc] = ("STUDY" + "\t" + study_acc + "\n" + \
										 "SAMPLE" + "\t" + sample_acc + "\n" + \
										 "NAME" + "\t" + experiment_name + "\n" + \
										 "PLATFORM" + "\t" + platform + "\n" + \
										 "LIBRARY_SOURCE" + "\t" + library_source + "\n" + \
										 "LIBRARY_SELECTION" + "\t" + library_selection + "\n" + \
										 "LIBRARY_STRATEGY" + "\t" + library_strategy + "\n" + \
										 fastq_string + "\n" )

			accessions.add(sample_acc)

			mapping_file_format.write("\t".join([study_acc, sample_acc, experiment_name, platform, library_source, library_selection, library_strategy, fastq_1, fastq_2]) + "\n")

for manifest in manifest_jobs:
	manifest_file = open(os.path.join(manifest_dir, manifest) + ".manifest", 'w')
	manifest_file.write(manifest_jobs[manifest])
	manifest_file.close()

acc_file = open("accessions.txt", "w")
for acc in sorted(accessions):
	acc_file.write(acc + "\n")
acc_file.close()

mapping_file_format.close()
